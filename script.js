/*ОБработчик событий - функция, которая "вешается"
на слушатель события, и выполняется, когда событие определенного типа случается на данном элементе.*/

function createForm() {

    const input = document.createElement('input');
    const label = document.createElement('label');
    const container = document.createElement('section');

    container.classList.add('container');

    input.setAttribute('type', 'number');
    input.setAttribute('id', 'price');

    label.setAttribute('for', 'price');
    label.innerText = 'Price';

    input.onfocus = () => {
        input.classList.add('outline-green');
    };

    input.onblur = () => {
        if (+input.value < 0 || input.value === '') {
            input.classList.add('outline-red');
            input.value = '';

            const warningMessage = document.createElement('div');
            warningMessage.classList.add('warning');
            warningMessage.innerText = 'Please enter correct price';
            input.after(warningMessage);
        } else {
            input.classList.remove('outline-green');
            createSpan();
        }
    };

    const createSpan = function () {
        functionWorked = true;
        input.classList.add('font-color');

        const span = document.createElement('span');
        span.innerHTML = `Текущая цена, &#36: ${input.value}`;

        const btnRemove = document.createElement('button');
        btnRemove.classList.add('btnRemove');
        btnRemove.innerText = 'X';

        btnRemove.addEventListener('click', () => {
            btnRemove.remove();
            span.remove();
            input.value = '';
            input.classList.remove('font-color');
        });

        span.appendChild(btnRemove);
        container.prepend(span);
    };

    container.prepend(input);
    container.prepend(label);

    document.body.prepend(container);
}

document.addEventListener('load', createForm());